Drupal Fedora API module README.txt
==============================================================================

This is an API module that provides methods for accessing the Fedora digital
object repository (http://fedora-commons.org/) API-A (access) and API-M
(management) APIs for use by other Drupal modules.

The module provides an admin settings page to specify the location of a Fedora
version 3.3 server and the username and password of a Fedora user with
appropriate permssions to access repository objects. It's up to the site
maintainer to ensure that Fedora is properly installed and running and
accessible from the Drupal server instance.

Developer usage
-----------------------------------------------------------------------------

The module doesn't do anything on its own, but provides a full interface for
developers to easily access the web services provided by the Fedora web app
without having to construct their own HTTP requests.

Note about the Fedora Name
-----------------------------------------------------------------------------
Not to be confused with the Linux distribution of the same name - the Fedora
repository software is a completely separate project and has no connection
whatsoever with the Linux distribution or with Red Hat, Inc.
